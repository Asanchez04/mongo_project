from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class Tema:
    uuid: str
    name: str
    description: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class Documental:
    uuid: str  # UUID
    name: str
    duration: str
    tema: Tema  # puede llevar uuid o id de Tema

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_Tema(self):
        return self.tema.name
