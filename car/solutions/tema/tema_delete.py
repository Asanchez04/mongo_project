from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="tema",
        record_id="62af8b60c0c73e4359e6d859",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema", limit_value=20)
)
