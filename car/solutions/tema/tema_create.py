from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv


mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema"
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="tema",
        record={"name": "Futbol", "description": "Temas sobre la cultura del futbol"}, 
                
                
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema", limit_value=20)
)


# [
#                 {"name": "Comics", "description": "Temas del mundo de Marvel"}, 
#                 {"name": "Drogas", "description": "Temas sobre la venta de drogas"},
#                 {"name": "Cocina", "description": "Temas sobre el mundo culinario"},
#                 {"name": "Anime", "description": "Temas sobre la cultura del anime"},
#                 ]