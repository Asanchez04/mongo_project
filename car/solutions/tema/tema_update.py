from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema"
    )
)

print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="tema",
        record_query={"_id": ObjectId("62af8ba1369273d34a3de32a")},
        record_new_value={"name": "Drogas y Narcotrafico", "description": "Temas sobre la venta de drogas y el narcotrafico"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="tema", limit_value=20)
)