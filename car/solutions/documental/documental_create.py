from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="documental"
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="documental",
        record={"name": "Cocina culinaria en Brasil", "duracion": "150 minutos", "tema": "62af8bb98f76638d7660947b"}, 
                
                
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="documental", limit_value=20)
)



# [
#                 {"name": "Pelea contra las drogas", "duracion": "140 minutos", "tema": "62af8ba1369273d34a3de32a"}, 
#                 {"name": "Drogas", "description": "Temas sobre la venta de drogas"},
#                 {"name": "Cocina", "description": "Temas sobre el mundo culinario"},


# 62af8bc56dc63d3b7ce2822d
# 62af8bb98f76638d7660947b
#                 
#    